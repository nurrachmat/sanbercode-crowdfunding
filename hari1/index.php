<?php

trait Hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        return $this->nama . ' sedang ' . $this->keahlian;
    }
}

trait Fight
{
    public $attackPower;
    public $defencePower;

    public function serang($h1, $h2)
    {
        $h2->darah = $h2->darah - ($h1->attackPower / $h2->defencePower);
        return $h1->nama . ' sedang menyerang ' . $h2->nama;
    }

    public function diserang($h1, $h2)
    {
        $h1->darah = $h1->darah - ($h2->attackPower / $h1->defencePower);
        return $h1->nama . ' sedang diserang ' . $h2->nama;
    }
}

class Elang
{
    use Hewan, Fight;

    public function __construct($nama, $jumlahKaki = 2, $keahlian = "terbang tinggi", $attackPower = 10, $defencePower = 5)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan()
    {
        return 'Nama hewan: ' . $this->nama . '<br>Darah: ' . $this->darah . '<br>Jumlah kaki: ' . $this->jumlahKaki . '<br>Keahlian: ' . $this->keahlian . '<br>attackPower:' . $this->attackPower . '<br>defencePower: ' . $this->defencePower . '<br>Jenis hewan: Elang';
    }
}

class Harimau
{
    use Hewan, Fight;

    public function __construct($nama, $jumlahKaki = 4, $keahlian = "lari cepat", $attackPower = 7, $defencePower = 8)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan()
    {
        return 'Nama hewan: ' . $this->nama . '<br>Darah: ' . $this->darah . '<br>Jumlah kaki: ' . $this->jumlahKaki . '<br>Keahlian: ' . $this->keahlian . '<br>AttackPower:' . $this->attackPower . '<br>defencePower: ' . $this->defencePower . '<br>Jenis hewan: Harimau<br>';
    }
}

echo "<h2>Tugas ke-1 - rachmat.nur@gmail.com</h2><hr>";
$objElang = new Elang("elang_1");
echo $objElang->getInfoHewan();
echo "<hr>";
echo $objElang->atraksi();
echo "<hr>";
$objHarimau = new Harimau("harimau_1");
echo $objHarimau->getInfoHewan();
echo "<hr>";
echo $objHarimau->atraksi();
echo "<hr>";
echo "<strong>Fight</strong>";
echo "<hr>";
echo $objElang->serang($objHarimau, $objElang);
echo "<hr>";
echo $objElang->diserang($objElang, $objHarimau);
echo "<hr>";
echo "<strong>getInfoHewan</strong>";
echo "<hr>";
echo $objElang->getInfoHewan();
echo "<hr>";
echo $objHarimau->getInfoHewan();
echo "<hr>";
echo $objElang->serang($objElang, $objHarimau);
echo "<hr>";
echo $objElang->serang($objElang, $objHarimau);
echo "<hr>";
echo $objElang->getInfoHewan();
echo "<hr>";
echo $objHarimau->getInfoHewan();
