<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;
use Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => Str::uuid(),
            'name' => Str::random(10),
            'email' => Str::random(10) . '@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => 'a656e20c-86d9-4c23-b72f-89a93c8240ca',
            'email_verified_at' => '2021-01-13 23:19:46',
        ]);

        DB::table('users')->insert([
            'id' => Str::uuid(),
            'name' => Str::random(10),
            'email' => Str::random(10) . '@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => 'a656e20c-86d9-4c23-b72f-89a93c8240ca',
        ]);

        DB::table('users')->insert([
            'id' => Str::uuid(),
            'name' => Str::random(10),
            'email' => Str::random(10) . '@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => '5940be91-dc33-4682-a44d-6f9c3b546734',
            'email_verified_at' => '2021-01-13 23:19:46',
        ]);

        DB::table('users')->insert([
            'id' => Str::uuid(),
            'name' => Str::random(10),
            'email' => Str::random(10) . '@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => '5940be91-dc33-4682-a44d-6f9c3b546734',
        ]);
    }
}
