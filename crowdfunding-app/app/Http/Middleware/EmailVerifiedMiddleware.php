<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
// use Auth;

class EmailVerifiedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        // if (Auth::user()->isEmailVerified()) {
        //     return $next($request);
        // }
        // abort(401);

        $user = auth()->user();
        if ($user->email_verified_at != null) {
            return $next($request);
        }

        return response()->json([
            'message' => 'Email Anda belum terverifikasi',
        ]);
    }
}
