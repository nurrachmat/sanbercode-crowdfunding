<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    // cara ke-1 pasang middleware
    // public function __construct()
    // {
    //     $this->middleware('dateMiddleware');
    // }

    public function test()
    {
        return 'berhasil masuk';
    }

    public function test1()
    {
        return 'berhasil masuk ke test1';
    }

    public function admin()
    {
        // dd(Auth::user());
        return 'Admin berhasil masuk';
    }

    public function route1()
    {
        // dd(Auth::user());
        return 'Email sudah diverifikasi';
    }

    public function route2()
    {
        // dd(Auth::user());
        return 'Admin berhasil masuk dan email sudah diverifikasi';
    }
}
