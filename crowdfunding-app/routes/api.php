<?php

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\Auth\RegisterController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });




Route::namespace('App\Http\Controllers\Auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::namespace('App\Http\Controllers\Article')->middleware('auth')->group(function () {
    Route::post('create-new-article', 'ArticleController@store');
    Route::patch('update-the-selected-article/{article}', 'ArticleController@update');
    Route::delete('delete-the-selected-article/{article}', 'ArticleController@destroy');
});

Route::get('articles/{article}', 'App\Http\Controllers\Article\ArticleController@show');

Route::get('articles', 'App\Http\Controllers\Article\ArticleController@index');

Route::get('user', 'App\Http\Controllers\UserController');
