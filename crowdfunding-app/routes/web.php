<?php

use App\Models\Role;
use App\Models\User;
use App\Models\Otp_code;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

use App\Http\Controllers\TestController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// cara ke-2 pasang middleware
// Route::get('/test', [TestController::class, 'test'])->middleware('dateMiddleware');

// cara ke-3 pasang middleware di dalam group
Route::middleware('dateMiddleware')->group(function () {
    Route::get('/test', [TestController::class, 'test']);
});

Route::get('/test1', [TestController::class, 'test1']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::middleware(['auth', 'emailVerified'])->group(function () {
//     Route::get('/route-1', [TestController::class, 'route1']);
// });

// Route::middleware(['auth', 'emailVerified', 'admin'])->group(function () {
//     Route::get('/admin', [TestController::class, 'admin']);
//     Route::get('/route-2', [TestController::class, 'route2']);
// });

Route::get('/route-1', function () {
    return 'masuk ke route-1';
})->middleware(['auth', 'email_verified']);

Route::get('/route-2', function () {
    return 'masuk ke route-2';
})->middleware(['auth', 'email_verified', 'admin']);



// Route::get('testing', function () {
//     $now = new \DateTime();

//     $role_id = Str::uuid();
//     $role = Role::create([
//         'id'    => $role_id,
//         'name'  => 'user'
//     ]);

//     $otp_code_id = Str::uuid();
//     $otp_code = Otp_code::create([
//         'id'            => $otp_code_id,
//         'otp_number'    => '1234',
//         'expired_at'    => $now
//     ]);

//     $user = User::create([
//         'id'        => Str::uuid(),
//         'name'          => 'Nur Rachmat',
//         'email'         => 'rachmat.nur@gmail.com',
//         'password'      => '123456',
//         'roles_id'      => $role->id,
//         'otp_codes_id'  => $otp_code->id
//     ]);

//     dd($user);
// });
